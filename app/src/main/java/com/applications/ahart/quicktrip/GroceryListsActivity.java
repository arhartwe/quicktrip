package com.applications.ahart.quicktrip;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.view.View;

import java.util.ArrayList;

public class GroceryListsActivity extends AppCompatActivity {

    Button add;
    ListView grocerylists;
    String exampleName = "grocery list example";
    GroceryList g = new GroceryList(exampleName);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_lists);

        add = (Button) findViewById(R.id.add);
        grocerylists = (ListView) findViewById(R.id.grocerylists);

        //Incomplete due to no backend
//        ArrayList<GroceryList> gLists = new ArrayList<>();
//        gLists.add(g);
//        GroceryListsAdapter groceryListsAdapter = new GroceryListsAdapter(GroceryListsActivity.this,
//                R.layout.grocery_list_content, gLists);
//        grocerylists.setAdapter(groceryListsAdapter);
    }

    public void navigateToCreateGroceryList(View view) {
        Intent intent = new Intent(this, CreateGroceryListActivity.class);
        startActivityForResult(intent, 1);
    }

    public void navigateToSampleList(View view) {
        Intent intent = new Intent(this, ViewGroceryListActivity.class);
        startActivityForResult(intent, 1);
    }
}
