package com.applications.ahart.quicktrip;

import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ViewGroceryListActivity extends AppCompatActivity {

    //Static data from app... this will change into user data
    private final String image_titles[] = {
            "apple.png",
            "banana.jpg",
            "kiwi.png",
            "oranges.jpg",
            "watermelon.jpg"
    };

    private final Integer image_ids[] = {
            R.drawable.apple,
            R.drawable.banana,
            R.drawable.kiwi,
            R.drawable.oranges,
            R.drawable.watermelon,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grocery_recycler_view);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.gallery);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(layoutManager);

        ArrayList<ImageCell> cells = prepareData();
        ImageAdapter adapter = new ImageAdapter(getApplicationContext(), cells);
        recyclerView.setAdapter(adapter);
    }

    private ArrayList<ImageCell> prepareData() {
        ArrayList<ImageCell> theimage = new ArrayList<>();

        for(int i = 0; i < image_titles.length; i++) {
            ImageCell cell = new ImageCell();
            cell.setTitle(image_titles[i]);
            cell.setImg(image_ids[i]);
            theimage.add(cell);
        }

        return theimage;
    }

}
