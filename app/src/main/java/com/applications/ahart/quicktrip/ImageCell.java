package com.applications.ahart.quicktrip;

public class ImageCell {

    private String title;
    private Integer img;

    public String getTitle() {
        return title;
    }

    public Integer getImg() {
        return img;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImg(Integer img) {
        this.img = img;
    }
}
