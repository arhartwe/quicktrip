package com.applications.ahart.quicktrip;

import java.util.ArrayList;

public class GroceryList {

    private String name;
    private ArrayList<Grocery> groceries;
//    private ArrayList<Friend> friends;

//    public GroceryList(String name, ArrayList<Grocery> groceries) {
    public GroceryList(String name) {
        this.name = name;
//        this.groceries = groceries;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Grocery> getGroceries() {
        return groceries;
    }

    public void setGroceries(ArrayList<Grocery> groceries) {
        this.groceries = groceries;
    }
}
