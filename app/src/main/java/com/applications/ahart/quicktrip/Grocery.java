package com.applications.ahart.quicktrip;

public class Grocery {

    private String title;
    private byte[] image;

    public Grocery(String title, byte[] image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
