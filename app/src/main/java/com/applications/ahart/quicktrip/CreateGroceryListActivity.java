package com.applications.ahart.quicktrip;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;

public class CreateGroceryListActivity extends AppCompatActivity {

    EditText listName;
    Button addFriends;
    Button create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_grocery_list);

        listName = (EditText) findViewById(R.id.listName);
        addFriends = (Button) findViewById(R.id.addFriends);
        create = (Button) findViewById(R.id.create);
    }

    public void navigateToGroceryLists(View v) {
        Intent intent = new Intent(this, GroceryListsActivity.class);
        startActivityForResult(intent, 1);
    }
}
