package com.applications.ahart.quicktrip;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.View;

import java.util.ArrayList;

/*
    This is incomplete due to no backend
 */
public class GroceryListsAdapter extends ArrayAdapter<GroceryList> {

    private Context context;
    int resource;
    private static class ViewHolder {
        Button groceryObject;
    }

    public GroceryListsAdapter(Context context, int resource, ArrayList<GroceryList> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String listName = getItem(position).getName();
        ViewHolder holder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();
            holder.groceryObject.setText(listName);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.groceryObject.setText(listName);
        return convertView;
    }

}
